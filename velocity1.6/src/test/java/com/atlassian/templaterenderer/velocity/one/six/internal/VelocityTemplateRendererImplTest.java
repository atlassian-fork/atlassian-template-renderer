package com.atlassian.templaterenderer.velocity.one.six.internal;

import com.atlassian.templaterenderer.TemplateContextFactory;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.templaterenderer.velocity.tests.AbstractVelocityRendererImplTest;

import java.util.Map;

public class VelocityTemplateRendererImplTest extends AbstractVelocityRendererImplTest {
    @Override
    protected TemplateRenderer createVelocityRenderer(TemplateContextFactory contextFactory,
                                                      ClassLoader classLoader,
                                                      String pluginKey,
                                                      Map<String, String> initProperties) {
        return new VelocityTemplateRendererImpl(classLoader, pluginKey, initProperties, contextFactory);
    }
}
