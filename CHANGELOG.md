# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [4.0.1]
### Changed
* velocity-htmlsafe bumped to 3.1.0 to bring SecureUberspector changes
* velocity bumped to 1.6.4-atlassian-19 to bring performance enhancement in SecureUberspector
* Moved to latest available platform 5.0.11

## [4.0.0]

### Changed
* Adds support for Java 11
* Now depends on Platform 5.0 being provided by the host product
* Removes dependence on atlassian-util-concurrent
